This projects aims to find Hearthstone cards that have mechanics. Cards info is fetched from [RapidAPI](https://rapidapi.com/omgvamp/api/hearthstone).
You need to create an account on RapidAPI to have the api key. Then, you can place it in src/secrets.json file.

The project is written in React Native with Typesript. It is created using Typescript template: `npx react-native init AppName --template react-native-template-typescript` You can find more about React Native and Typescript [here](https://reactnative.dev/docs/typescript).

After pulling the project, run `npm install` or `yarn` in the root directory to install the dependencies. Then, `npx react-native start` and `npx react-native run-android` to build in Android emulator. To build the app on iOS, run `npx pod-install ios` in the root directory. After that, run `npx react-native run-ios`. The application should be installed successfully.
