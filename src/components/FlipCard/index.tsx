import React, {useState} from 'react';
import {View, StyleSheet, Pressable} from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withTiming,
} from 'react-native-reanimated';
import CardFront from '../CardFront';
import {Card} from '../../context/UniqueCardsContext';
import CardBack from '../CardBack';

interface FlipCardProps {
  card: Card;
}

const FlipCard: React.FC<FlipCardProps> = ({card}) => {
  const flipValue = useSharedValue(0);
  const [isFlipped, setIsFlipped] = useState(false);

  const frontAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {rotateY: `${180 * flipValue.value}deg`},
        // {perspective: 1000},
      ],
    };
  });

  const backAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {rotateY: `${180 + 180 * flipValue.value}deg`},
        // {perspective: 1000}, // smooth effect?
      ],
    };
  });

  const handleFlip = () => {
    setIsFlipped(!isFlipped);
    flipValue.value = withTiming(isFlipped ? 0 : 1, {duration: 700});
  };

  return (
    <Pressable onPress={handleFlip}>
      <View style={[styles.container]}>
        <Animated.View style={[styles.card, frontAnimatedStyle]}>
          <CardFront
            image={(card.img ? card.img : undefined) as string}
            name={card.name}
          />
        </Animated.View>
        <Animated.View style={[styles.card, backAnimatedStyle]}>
          <CardBack {...card} />
        </Animated.View>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 550,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  card: {
    position: 'absolute',
    height: 550,
    backfaceVisibility: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 8,
  },
  text: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
});

export default FlipCard;
