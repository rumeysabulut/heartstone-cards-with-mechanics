import React from 'react';
import {View, ActivityIndicator} from 'react-native';

type LoaderProps = {
  loading?: boolean;
};

const Loader = (props: LoaderProps) => (
  <View>
    <ActivityIndicator
      size="large"
      animating={props.loading}
      color={'#999999'}
    />
  </View>
);
export default Loader;
