import React from 'react';
import {View, Text, StyleSheet, useWindowDimensions} from 'react-native';
import {Card} from '../../context/UniqueCardsContext';

interface CardBackProps extends Card {}

const CardBack = (props: Partial<CardBackProps>) => {
  const screenWidth = useWindowDimensions().width;

  return (
    <View style={[{height: 550, width: screenWidth}, styles.container]}>
      {Object.entries(props)
        .filter(item => !item[0].startsWith('img'))
        .map(([key, value]) => {
          let formattedValue = value;
          if (key === 'mechanics') {
            formattedValue = (value as {name: string}[]).reduce(
              (acc, item) => `${acc}${item.name}`,
              '',
            );
          }
          return (
            <View style={{flex: 1}}>
              <Text
                style={styles.textStyle}>{`${key} : ${formattedValue}`}</Text>
            </View>
          );
        })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderColor: '#D49851',
    borderWidth: 1,
    justifyContent: 'center',
    borderRadius: 10,
    margin: 10,
    padding: 10,
    backgroundColor: '#F9F6F6',
  },
  textStyle: {
    fontSize: 20,
    color: 'rgb(37, 37, 37)',
  },
  line: {
    backgroundColor: 'rgb(227, 220, 214)',
    height: 1,
    marginVertical: 10,
  },
});

export default CardBack;
