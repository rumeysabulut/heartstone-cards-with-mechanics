import React, {useState} from 'react';
import {Image, View, useWindowDimensions, StyleSheet, Text} from 'react-native';

import Loader from '../Loader';

interface CardFrontProps {
  image?: string; // not all cards have an image
  name: string;
}
const CardFront = (props: CardFrontProps) => {
  const [loading, setLoading] = useState(true);
  const screenWidth = useWindowDimensions().width;

  return (
    <View style={[styles.container, {width: screenWidth}]}>
      {props.image ? (
        <View>
          <Image
            style={{
              height: 550,
            }}
            source={{uri: props.image}}
            onLoadStart={() => setLoading(true)}
            onLoadEnd={() => setLoading(false)}
          />
          <Loader loading={loading} />
        </View>
      ) : (
        <View style={styles.central}>
          <Text style={styles.textStyle}>{props.name}</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderColor: '#EC7C12',
    borderWidth: 1,
    borderRadius: 10,
    margin: 10,
    height: 550,
    backgroundColor: '#F9F6F6',
  },
  central: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  textStyle: {
    fontSize: 36,
    fontWeight: '700',
    color: '#EC7C12',
  },
});

export default CardFront;
