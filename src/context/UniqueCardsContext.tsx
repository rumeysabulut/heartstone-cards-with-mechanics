import React, {useState} from 'react';

export type Mechanic = {
  name: string;
};
export interface Card extends Record<string, unknown> {
  cardId: string;
  dbfId: string;
  mechanics: Mechanic[];
  name: string;
}

export type MechanicNameCardIdMap = Record<string, string[]>;
export type CardIdCardsMap = Record<string, Card>;
type UniqueCardsContextProps = {
  uniqueCardsList: CardIdCardsMap;
  setUniqueCardsList: React.Dispatch<React.SetStateAction<CardIdCardsMap>>;
  mechanicsList: MechanicNameCardIdMap;
  setMechanicsList: React.Dispatch<React.SetStateAction<MechanicNameCardIdMap>>;
};

export const UniqueCardsDefaultValues = {
  uniqueCardsList: {
    '': {
      cardId: '',
      cardSet: '',
      dbfId: '',
      locale: '',
      mechanics: [{name: ''}],
      name: '',
      playerClass: '',
      text: '',
      type: '',
    },
  },
  setUniqueCardsList: () => {},
  mechanicsList: {'': []},
  setMechanicsList: () => {},
};

const UniqueCardsContext = React.createContext<UniqueCardsContextProps>(
  UniqueCardsDefaultValues,
);

export const UniqueCardsContextProvider: React.FC = ({children}) => {
  const [uniqueCardsList, setUniqueCardsList] = useState<CardIdCardsMap>({});
  const [mechanicsList, setMechanicsList] = useState<MechanicNameCardIdMap>({});
  return (
    <UniqueCardsContext.Provider
      value={{
        uniqueCardsList,
        setUniqueCardsList,
        mechanicsList,
        setMechanicsList,
      }}>
      {children}
    </UniqueCardsContext.Provider>
  );
};

export default UniqueCardsContext;
