import {useRef} from 'react';

const useDebounce = (fn: Function, delay: number) => {
  const timeoutRef = useRef<number>();
  const cbRef = useRef<Function>(fn);
  if (fn === cbRef.current) {
    return timeoutRef.current;
  }
  if (timeoutRef.current) {
    clearTimeout(timeoutRef.current);
  }
  cbRef.current = fn;
  timeoutRef.current = setTimeout(fn, delay) as unknown as number;
  return timeoutRef.current;
};

export default useDebounce;
