import React, {useState} from 'react';
import {useCallback} from 'react';
import {SafeAreaView, TextInput, StyleSheet, View} from 'react-native';
import useDebounce from '../../hooks/useDebounce';
import Loader from '../../components/Loader';
import CardList from '../CardList';
import secrets from '../../secrets.json';

const Search = () => {
  const [searchText, setSearchText] = useState('');
  const [list, setList] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);

  const handler = useCallback(() => {
    if (searchText === '') {
      setList([]);
      return;
    }
    setLoading(true);
    fetch(
      `https://omgvamp-hearthstone-v1.p.rapidapi.com/cards/search/${searchText}`,
      {
        method: 'GET',
        headers: {
          'x-rapidapi-key': secrets['x-rapidapi-key'],
          'x-rapidapi-host': 'omgvamp-hearthstone-v1.p.rapidapi.com',
        },
      },
    )
      .then(async response => {
        const resp = await response.json();
        setList(resp);
        setLoading(false);
      })
      .catch(err => {
        console.error(err);
      });
  }, [searchText]);
  useDebounce(handler, 1000);

  console.log(list);

  return (
    <SafeAreaView>
      <TextInput
        placeholder={'Type card name to search'}
        value={searchText}
        onChangeText={setSearchText}
        style={styles.input}
      />
      <View>
        {loading ? <Loader loading={loading} /> : null}
        <CardList cardList={list} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 10,
    fontSize: 16,
  },
});

export default Search;
