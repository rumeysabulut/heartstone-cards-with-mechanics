import React, {useEffect, useState, useContext} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  Pressable,
} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';
import {RootStackParamList} from '../RootStackParamsList';
import secrets from '../../secrets.json';
import UniqueCardsContext, {
  Card,
  MechanicNameCardIdMap,
  CardIdCardsMap,
} from '../../context/UniqueCardsContext';
import Loader from '../../components/Loader';

type CardAccT = {
  [name: string]: string[];
};

type mechanicsListNavigationProps = StackNavigationProp<
  RootStackParamList,
  'MechanicsList'
>;

const MechanicsList = () => {
  const navigation = useNavigation<mechanicsListNavigationProps>();
  const CardListContext = useContext(UniqueCardsContext);
  const [isLoading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    fetch('https://omgvamp-hearthstone-v1.p.rapidapi.com/cards', {
      method: 'GET',
      headers: {
        'x-rapidapi-key': secrets['x-rapidapi-key'],
        'x-rapidapi-host': 'omgvamp-hearthstone-v1.p.rapidapi.com',
      },
    })
      .then(async response => {
        const resp = await response.json();
        const uniqueCardIds: CardIdCardsMap = {};
        const cardTypes: Card[][] = Object.values(resp);
        const mechanicsResult: MechanicNameCardIdMap = cardTypes.reduce(
          (acc, items) =>
            items.reduce((cardAcc: CardAccT, card) => {
              if ('mechanics' in card) {
                if (!(card.cardId in uniqueCardIds)) {
                  uniqueCardIds[card.cardId] = card;
                }
                card.mechanics.reduce((mechanicAcc, mechanic) => {
                  if (mechanic.name in cardAcc) {
                    mechanicAcc[mechanic.name].push(card.cardId);
                  } else {
                    mechanicAcc[mechanic.name] = [card.cardId];
                  }
                  return mechanicAcc;
                }, cardAcc);
              }
              return cardAcc;
            }, acc),
          {},
        );
        CardListContext.setMechanicsList(mechanicsResult);
        CardListContext.setUniqueCardsList(uniqueCardIds);
      })
      .catch(error => console.error(error))
      .finally(() => setLoading(false));
  }, [CardListContext]);

  const renderItem = ({item}: {item: string}) => (
    <View>
      <Pressable
        onPress={() => {
          navigation.navigate('CardList', {
            mechanicName: item,
          });
        }}
        style={({pressed}) => [
          {backgroundColor: pressed ? 'rgb(227, 220, 214)' : 'white'},
        ]}>
        <Text style={styles.itemText}>{item}</Text>
      </Pressable>
      <View style={styles.line} />
    </View>
  );

  const ListOfMechanics = () => (
    <SafeAreaView style={[styles.container, styles.listContainer]}>
      <FlatList
        data={Object.keys(CardListContext.mechanicsList)}
        renderItem={renderItem}
        keyExtractor={item => item}
      />
    </SafeAreaView>
  );

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={[styles.container, styles.central]}>
          <Text style={styles.loaderText}>
            {'Fetching Cards with Mechanics'}
          </Text>
          <Loader />
        </View>
      ) : (
        <ListOfMechanics />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    marginTop: StatusBar.currentHeight || 0,
    marginHorizontal: 10,
  },
  central: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    backgroundColor: 'rgb(227, 220, 214)',
    height: 1,
    marginVertical: 10,
  },
  itemText: {
    fontSize: 16,
    margin: 10,
  },
  loaderText: {
    fontSize: 20,
    color: '#999999',
    marginBottom: 20,
  },
});

export default MechanicsList;
