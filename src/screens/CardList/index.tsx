import React, {useEffect, useContext, useState, useLayoutEffect} from 'react';
import {
  FlatList,
  SafeAreaView,
  Pressable,
  Image,
  StyleSheet,
  View,
  ListRenderItemInfo,
} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {useNavigation, useRoute, RouteProp} from '@react-navigation/native';

import {RootStackParamList} from '../RootStackParamsList';
import UniqueCardsContext, {Card} from '../../context/UniqueCardsContext';
import FlipCard from '../../components/FlipCard';

const searchIcon = require('../../assets/search.png');

type CardListRouteProp = RouteProp<RootStackParamList, 'CardList'>;
type CardListNavigationProps = StackNavigationProp<
  RootStackParamList,
  'CardList'
>;

type CardListProps = {
  cardList?: Card[];
};

const ItemSeparator = () => <View style={styles.separator} />;

const CardList = (props: CardListProps) => {
  const navigation = useNavigation<CardListNavigationProps>();
  const route = useRoute<CardListRouteProp>();
  const CardsListContext = useContext(UniqueCardsContext);

  const [chosenCards, setChosenCards] = useState<Card[]>([]);

  useEffect(() => {
    if (props.cardList) {
      setChosenCards(props.cardList);
      return;
    }
    const chosenCardIds =
      CardsListContext.mechanicsList[route.params.mechanicName];
    const cardsInfo = chosenCardIds.map(id => {
      return CardsListContext.uniqueCardsList[id];
    });
    setChosenCards(cardsInfo);
  }, [
    CardsListContext.mechanicsList,
    CardsListContext.uniqueCardsList,
    props.cardList,
    route.params,
  ]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Pressable
          onPress={() => {
            navigation.navigate('Search');
          }}>
          <Image style={styles.searchIcon} source={searchIcon} />
        </Pressable>
      ),
    });
  }, [navigation]);

  const renderItem = (card: ListRenderItemInfo<Card>) => {
    return <FlipCard key={card.item.cardId} card={card.item} />;
  };

  return (
    <SafeAreaView>
      <FlatList
        data={chosenCards}
        renderItem={renderItem}
        keyExtractor={item => item.cardId}
        ItemSeparatorComponent={ItemSeparator}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  searchIcon: {
    width: 30,
    height: 30,
    marginRight: 20,
  },
  text: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  separator: {
    height: 16, // Adjust the height as per your spacing requirement
  },
});

export default CardList;
