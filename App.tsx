import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import MechanicsList from './src/screens/MechanicsList';
import CardList from './src/screens/CardList';
import Search from './src/screens/Search';
import {RootStackParamList} from './src/screens/RootStackParamsList';
import {UniqueCardsContextProvider} from './src/context/UniqueCardsContext';

const Stack = createStackNavigator<RootStackParamList>();

const App = () => {
  return (
    <UniqueCardsContextProvider>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="MechanicsList"
          screenOptions={{
            cardStyle: {backgroundColor: 'white'},
          }}>
          <Stack.Screen
            name="MechanicsList"
            component={MechanicsList}
            options={{
              title: 'Mechanics List',
              headerTitleStyle: {
                fontSize: 20,
              },
            }}
          />
          <Stack.Screen
            name="CardList"
            component={CardList}
            options={{
              title: 'Cards List',
              headerTitleStyle: {
                fontSize: 18,
              },
            }}
          />
          <Stack.Screen
            name="Search"
            component={Search}
            options={{
              title: 'Search',
              headerTitleStyle: {
                fontSize: 18,
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </UniqueCardsContextProvider>
  );
};

export default App;
